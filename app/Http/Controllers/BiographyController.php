<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biography;
use Validator;
use View;
class BiographyController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('biography.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        Validator::make($request->all(), [
            'name' => 'required|min:5|max:30',
            'phone' => 'required|digits_between:11,15',
            'email' => 'required|email|max:50',
            'gender' => 'required',
            'dob' => 'required|date|before:18 years ago',
            'biography' => 'required|min:10|max:100',
            'file' => 'file|image'
        ])->validate();
        $biography = new Biography($request->input());
        $biography->dob = date('Y-m-d', strtotime($request->input('dob')));
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path() . '/uploads/';
            $file->move($destinationPath, $fileName);
            $biography->file = $fileName;
        }
        $biography->save();
        return View::make('biography.show', ['biography' => $biography->attributesToArray(), 'message' => 'Biography has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
