<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Laravel 5.5 Biography </title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body>
        <div class="container">
            <h2>Biography</h2><br  />
            @if($message)
            {{$message}}
            @endif

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Name:</label>
                    {{{$biography['name']}}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="phone">Phone:</label>
                    {{{$biography['phone']}}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="email">Email:</label>
                    {{{$biography['email']}}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="gender">Gender:</label>
                    <!--<div class="radio-inline">-->
                    {{{$biography['gender']}}}
                    <!--</div>-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4 input-group date">
                    <label for="dob">DoB:</label>
                    {{{$biography['dob']}}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="biography">Biography:</label>
                    {{{$biography['biography']}}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="file">Image:</label>
                    <img src="{{ url('/') }}/uploads/{{{$biography['file']}}}" />
                </div>
            </div>


        </div>

    </body>


</html>